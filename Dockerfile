FROM node:22.2.0-alpine

ENV TZ="Asia/Krasnoyarsk"

WORKDIR /app

COPY package*.json ./

RUN yarn

COPY . .

RUN yarn run build

CMD ["node", "dist/main"]