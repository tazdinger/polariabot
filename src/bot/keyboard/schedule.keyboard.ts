export const scheduleKeyboard = [
  [
    { text: 'Текущее', callback_data: 'current' },
    { text: 'Удалить', callback_data: 'delete' },
  ],
  [{ text: 'Назад', callback_data: 'back' }],
];

export const scheduleKeyboardWithoutView = [[{ text: 'Назад', callback_data: 'back' }]];
