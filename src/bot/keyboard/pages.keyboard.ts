export const pagesKeyboard = [
  [
    { text: 'Предыдущая', callback_data: 'previous' },
    { text: 'Следующая', callback_data: 'next' },
  ],
  [{ text: 'Назад', callback_data: 'backToPortfolio' }],
];

export const onePagesKeyboard = [[{ text: 'Назад', callback_data: 'backToPortfolio' }]];
