export const sketchKeyboard = [
  [{ text: 'Просмотр', callback_data: 'view' }],
  [
    { text: 'Удалить', callback_data: 'delete' },
    { text: 'Назад', callback_data: 'back' },
  ],
];

export const sketchKeyboardWithoutView = [[{ text: 'Назад', callback_data: 'back' }]];
