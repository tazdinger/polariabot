import { Action, Ctx, InjectBot, On, Scene, SceneEnter, SceneLeave } from 'nestjs-telegraf';
import { Context, Telegraf } from 'telegraf';
import { CtxWithPhoto } from '../type/photos-ctx.type';
import { Logger } from '@nestjs/common';
import { BotService } from '../bot.service';
import { mainKeyboard } from '../keyboard/main.keyboard';
import { SceneType } from '../type/scene.type';
import { folder } from 'src/common/folder-path';
import { onePagesKeyboard, pagesKeyboard } from '../keyboard/pages.keyboard';
import { sketchKeyboard } from '../keyboard/sketch.keyboard';
import { portfolioKeyboard, portfolioKeyboardWithoutView } from '../keyboard/portfolio.keyboard';
import { CtxSession } from '../type/ctx-session.type';

@Scene('portfolio')
export class PortfolioScene {
  constructor(
    private readonly botService: BotService,
    @InjectBot() private readonly bot: Telegraf,
  ) {}

  private ON_PAGE = 6;

  @SceneEnter()
  public async sceneEnter(@Ctx() ctx: Context & CtxSession): Promise<void> {
    ctx.session.mediaMessageId = [];
    ctx.session.lastMessage = await ctx
      .editMessageText('Твои работы', {
        reply_markup: {
          inline_keyboard: portfolioKeyboard,
        },
      })
      .then((messId: any) => messId.message_id);
  }

  @On('photo')
  public async save(@Ctx() ctx: Context & CtxWithPhoto & CtxSession) {
    await ctx.deleteMessage(ctx.session.lastMessage).catch(() => null);
    Logger.log('Loading image...');
    const photos = ctx.message.photo;
    await this.botService.saveFile(photos, folder.portfolio);

    if (ctx.session.isReplySent) {
      return;
    }
    ctx.session.isReplySent = true;
    ctx.session.lastMessage = await ctx
      .reply('Выберите пункт', {
        reply_markup: { inline_keyboard: portfolioKeyboard },
      })
      .then((messId) => messId.message_id);

    await this.delay(5000);
    ctx.session.isReplySent = false;
  }

  delay(ms: number): Promise<void> {
    return new Promise((resolve) => setTimeout(resolve, ms));
  }

  @Action('delete')
  public async delete(@Ctx() ctx: Context & CtxSession) {
    await this.botService.deleteFiles(folder.portfolio);
    ctx.session.lastMessage = await ctx
      .editMessageText('Загрузи сюда свои работы', {
        reply_markup: { inline_keyboard: portfolioKeyboardWithoutView },
      })
      .then((messId: any) => messId.message_id);
  }

  @Action('view')
  public async getPortfolio(@Ctx() ctx: Context & CtxSession & CtxSession) {
    await ctx.deleteMessages(ctx.session.mediaMessageId).catch(() => null);
    await ctx.deleteMessage(ctx.session.lastMessage).catch(() => null);
    const images = await this.botService.getFiles(ctx, folder.portfolio);
    if (images.length === 0) {
      ctx.session.lastMessage = await ctx
        .reply('Загрузи сюда свои работы', {
          reply_markup: { inline_keyboard: portfolioKeyboardWithoutView },
        })
        .then((messId) => messId.message_id);
      return;
    }
    const totalPages = this.getTotalPages(images);
    if (totalPages === 1) {
      const arr = await ctx.replyWithMediaGroup(images).then((messId: any) => messId.message_id);
      ctx.session.mediaMessageId = arr;
      ctx.session.lastMessage = await ctx
        .reply(`Страница 1`, {
          reply_markup: { inline_keyboard: onePagesKeyboard },
        })
        .then((messId) => messId.message_id);
      return;
    }
    const currentPage = ctx.session.page >= totalPages ? totalPages - 1 : ctx.session.page || 0;
    ctx.session.page = currentPage;
    const response = await this.getPage(currentPage, images);
    const mediaMessageIds = await ctx.replyWithMediaGroup(response);
    const tmp = [];
    ctx.session.mediaMessageId = [];
    mediaMessageIds.map((image) => tmp.push(image.message_id));
    await ctx.session.mediaMessageId.push(...tmp);
    ctx.session.lastMessage = await ctx
      .reply(`Страница ${currentPage + 1}`, {
        reply_markup: { inline_keyboard: pagesKeyboard },
      })
      .then((messId) => messId.message_id);
  }

  private async getPage(page: number, content: any[]) {
    const startIndex = page * this.ON_PAGE;
    const endIndex = startIndex + this.ON_PAGE;
    return content.slice(startIndex, endIndex);
  }

  private getTotalPages(content: any[]): number {
    return Math.ceil(content.length / this.ON_PAGE);
  }

  @Action('next')
  private async nextPage(@Ctx() ctx: Context & CtxSession) {
    ctx.session.page = ctx.session.page + 1;
    await this.getPortfolio(ctx);
  }

  @Action('previous')
  private async previousPage(@Ctx() ctx: Context & CtxSession) {
    ctx.session.page = ctx.session.page <= 0 ? 0 : ctx.session.page - 1;
    await this.getPortfolio(ctx);
  }

  @Action('backToPortfolio')
  public async backToPortfolio(@Ctx() ctx: Context & CtxSession) {
    ctx.session.lastMessage = await ctx
      .editMessageText('Выберите пункт', {
        reply_markup: {
          inline_keyboard: sketchKeyboard,
        },
      })
      .then((messId: any) => messId.message_id);
  }

  @Action('back')
  public async onCancel(@Ctx() ctx: Context & SceneType) {
    await ctx.editMessageText('Выберите пункт', {
      reply_markup: {
        inline_keyboard: mainKeyboard,
      },
    });
    await ctx.scene.leave();
  }

  @SceneLeave()
  private async onSceneLeave() {
    Logger.log('Вышел из портфолио');
  }
}
