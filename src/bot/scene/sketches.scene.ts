import { sketchKeyboard, sketchKeyboardWithoutView } from '../keyboard/sketch.keyboard';
import { Action, Ctx, InjectBot, On, Scene, SceneEnter, SceneLeave } from 'nestjs-telegraf';
import { Context, Telegraf } from 'telegraf';
import { CtxWithPhoto } from '../type/photos-ctx.type';
import { FileService } from 'src/file/file.service';
import { BotService } from '../bot.service';
import { Logger } from '@nestjs/common';
import { mainKeyboard } from '../keyboard/main.keyboard';
import * as path from 'path';
import { SceneType } from '../type/scene.type';
import { MediaGroup } from 'telegraf/typings/telegram-types';
import { CtxSession } from '../type/ctx-session.type';

@Scene('sketches')
export class SketchScene {
  constructor(
    private readonly fileService: FileService,
    @InjectBot() private readonly bot: Telegraf,
    private readonly botService: BotService,
  ) {}

  private STATIC = path.join(__dirname, '..', '..', '..', 'static', 'sketches');

  @SceneEnter()
  async onSceneEnter(@Ctx() ctx: Context & CtxSession) {
    ctx.session.isReplySent = false;
    ctx.session.lastMessage = await ctx
      .editMessageText('Отправь эскизы для сохранения', {
        reply_markup: {
          inline_keyboard: sketchKeyboard,
        },
      })
      .then((messId: any) => messId.message_id);
  }

  @On('photo')
  public async save(@Ctx() ctx: Context & CtxWithPhoto & CtxSession) {
    await ctx.deleteMessage(ctx.session.lastMessage).catch(() => null);
    Logger.log('Loading image...');
    const photos = ctx.message.photo;
    await this.botService.saveFile(photos, this.STATIC);

    if (ctx.session.isReplySent) {
      return;
    }
    ctx.session.isReplySent = true;
    ctx.session.lastMessage = await ctx
      .reply('Выбери пункт', {
        reply_markup: { inline_keyboard: sketchKeyboard },
      })
      .then((messId) => messId.message_id);

    setTimeout(() => {
      ctx.session.isReplySent = false;
    }, 5000);
  }

  @Action('delete')
  public async delete(@Ctx() ctx: Context & CtxSession) {
    await this.botService.deleteFiles(this.STATIC);
    ctx.session.lastMessage = await ctx
      .editMessageText('Загрузи эскизы, закинув их в бота', {
        reply_markup: { inline_keyboard: sketchKeyboardWithoutView },
      })
      .then((messId: any) => messId.message_id);
  }

  @Action('back')
  async onCancel(@Ctx() ctx: Context & SceneType) {
    await ctx.editMessageText('Выбери пункт', {
      reply_markup: {
        inline_keyboard: mainKeyboard,
      },
    });
    await ctx.scene.leave();
  }

  @SceneLeave()
  async onSceneLeave() {
    Logger.log('Вышел из скетчей');
  }

  @Action('view')
  public async getSketch(ctx: Context & CtxSession): Promise<void> {
    const media = await this.botService.getFiles(ctx, this.STATIC);
    if (media.length === 0) {
      await ctx.editMessageText(
        'Эскизов на данный момент нет :(\nЗагрузи эскизы, закинув их в бота',
        {
          reply_markup: { inline_keyboard: sketchKeyboardWithoutView },
        },
      );
      return;
    }
    await ctx.deleteMessage();
    await ctx.replyWithMediaGroup(media as MediaGroup);
    ctx.session.lastMessage = await ctx
      .reply('Удалить эскизы?', {
        reply_markup: { inline_keyboard: sketchKeyboard },
      })
      .then((messId: any) => messId.message_id);
  }
}
