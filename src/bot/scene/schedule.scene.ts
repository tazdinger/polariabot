import { Action, Ctx, On, Scene, SceneEnter, SceneLeave } from 'nestjs-telegraf';
import { FileService } from 'src/file/file.service';
import { Context } from 'telegraf';
import { scheduleKeyboard, scheduleKeyboardWithoutView } from '../keyboard/schedule.keyboard';
import { CtxWithPhoto } from '../type/photos-ctx.type';
import { Logger } from '@nestjs/common';
import { BotService } from '../bot.service';
import { mainKeyboard } from '../keyboard/main.keyboard';
import { SceneType } from '../type/scene.type';
import { MediaGroup } from 'telegraf/typings/telegram-types';
import { formatRussianDate } from 'src/common/date-util';
import { CtxSession } from '../type/ctx-session.type';
import { folder } from 'src/common/folder-path';

@Scene('schedule')
export class ScheduleScene {
  constructor(
    private readonly fileService: FileService,
    private readonly botService: BotService,
  ) {}

  private async onModuleInit() {
    const arr = await this.fileService.getFiles(folder.schedule);
    if (arr.length > 0) {
      this.existsSchedule = true;
    }
  }

  private existsSchedule: boolean = false;

  @SceneEnter()
  public async sceneEnter(@Ctx() ctx: Context & CtxSession): Promise<void> {
    const messId = await ctx
      .editMessageText('Твое расписание', {
        reply_markup: {
          inline_keyboard: scheduleKeyboard,
        },
      })
      .then((messId: any) => messId.message_id);
    ctx.session.lastMessage = messId;
  }

  @On('photo')
  public async save(@Ctx() ctx: Context & CtxSession & CtxWithPhoto) {
    await ctx.deleteMessage(ctx.session.lastMessage).catch(() => null);
    if (this.existsSchedule) {
      ctx.deleteMessage();
      return;
    }
    this.existsSchedule = true;
    Logger.log('Loading image...');
    const photos = ctx.message.photo;
    await this.botService.saveFile(photos, folder.schedule);
    ctx.session.lastMessage = await ctx
      .reply('Выберите пункт', {
        reply_markup: { inline_keyboard: scheduleKeyboard },
      })
      .then((mesId) => mesId.message_id);
  }

  @Action('delete')
  public async delete(@Ctx() ctx: Context & CtxSession) {
    await this.botService.deleteFiles(folder.schedule);
    this.existsSchedule = false;
    ctx.session.lastMessage = await ctx
      .editMessageText('Загрузи свежее расписание', {
        reply_markup: { inline_keyboard: scheduleKeyboardWithoutView },
      })
      .then((messId: any) => messId.message_id);
  }

  @Action('current')
  public async getSchedule(ctx: Context): Promise<void> {
    const media: any[] = await this.botService.getFiles(ctx, folder.schedule);
    if (media.length === 0) {
      await ctx.editMessageText('На данный момент расписания нет', {
        reply_markup: { inline_keyboard: scheduleKeyboardWithoutView },
      });
      return;
    }
    const createdAt = await this.fileService
      .dateOfCreate(media[0].media.source)
      .then((date) => formatRussianDate(date));
    media[0].caption = `Актуально: ${createdAt}`;
    await ctx.deleteMessage();
    await ctx.replyWithMediaGroup(media as MediaGroup);
    await ctx.reply('Удалить расписание?', {
      reply_markup: { inline_keyboard: scheduleKeyboard },
    });
  }

  @Action('back')
  async onCancel(@Ctx() ctx: Context & SceneType) {
    await ctx.editMessageText('Выберите пункт', {
      reply_markup: {
        inline_keyboard: mainKeyboard,
      },
    });
    await ctx.scene.leave();
  }

  @SceneLeave()
  async onSceneLeave() {
    Logger.log('Вышел из расписания');
  }
}
