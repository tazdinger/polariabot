import { Module } from '@nestjs/common';
import { BotService } from './bot.service';
import { BotProvider } from './bot.provider';
import { TelegrafModule } from 'nestjs-telegraf';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { FileService } from 'src/file/file.service';
import { MiddlewareModule } from 'src/middleware/middleware.module';
import { WhitelistMiddleware as WhitelistMiddleware } from 'src/middleware/whitelist.middleware';
import { session } from 'telegraf';
import { ScheduleScene } from './scene/schedule.scene';
import { SketchScene } from './scene/sketches.scene';
import { PortfolioScene } from './scene/portfolio.scene';
import { Redis } from '@telegraf/session/redis';

@Module({
  imports: [
    MiddlewareModule,
    TelegrafModule.forRootAsync({
      imports: [ConfigModule, MiddlewareModule],
      inject: [ConfigService, WhitelistMiddleware],
      useFactory: (configService: ConfigService, whitelistMiddleware: WhitelistMiddleware) => ({
        token: configService.get<string>('TOKEN'),
        middlewares: [
          whitelistMiddleware.use,
          session({
            store: Redis({
              url: configService.get<string>('REDIS_URL'),
              config: { database: 1 },
            }),
          }),
        ],
      }),
    }),
  ],
  controllers: [],
  providers: [BotProvider, BotService, FileService, SketchScene, ScheduleScene, PortfolioScene],
  exports: [BotProvider, BotService],
})
export class BotModule {}
