import { Context } from 'telegraf';
import { BotService } from './bot.service';
import { Action, Ctx, Start, Update } from 'nestjs-telegraf';
import { SceneType } from './type/scene.type';

@Update()
export class BotProvider {
  constructor(private readonly botService: BotService) {}

  @Start()
  public greetings(@Ctx() ctx: Context): void {
    this.botService.greetings(ctx);
  }

  @Action('sketch')
  public goSketch(@Ctx() ctx: Context & SceneType) {
    ctx.scene.enter('sketches');
  }

  @Action('schedule')
  public goSchedule(@Ctx() ctx: Context & SceneType) {
    ctx.scene.enter('schedule');
  }

  @Action('portfolio')
  public goPortfolio(@Ctx() ctx: Context & SceneType) {
    ctx.scene.enter('portfolio');
  }
}
