import { Injectable, Logger } from '@nestjs/common';
import { InjectBot } from 'nestjs-telegraf';
import { Context, Telegraf } from 'telegraf';
import { Photos } from './type/photos-ctx.type';
import * as path from 'path';
import { FileService } from 'src/file/file.service';
import { mainKeyboard } from './keyboard/main.keyboard';
import { InputMediaPhoto } from 'telegraf/typings/core/types/typegram';

@Injectable()
export class BotService {
  constructor(
    @InjectBot() private readonly bot: Telegraf,
    private readonly fileService: FileService,
  ) {}

  public async greetings(ctx: Context): Promise<void> {
    await ctx.reply('Что у нас есть!', {
      reply_markup: {
        inline_keyboard: mainKeyboard,
      },
    });
  }

  public async getFiles(ctx: Context, folderPath: string): Promise<InputMediaPhoto[]> {
    const files = await this.fileService.getFiles(folderPath).catch(() => {
      return [] as string[];
    });
    if (files.length === 0) {
      return [];
    }
    const media: InputMediaPhoto[] = files.map((filePath) => {
      return { type: 'photo', media: { source: path.join(folderPath, filePath) } };
    });
    return media;
  }

  public async saveFile(photos: Photos, dist: string) {
    const url = await this.extractImageUrl(photos);
    await this.fileService.saveFromUrl(url, dist);
  }

  public async deleteFiles(folderPath: string) {
    await this.fileService.deleteFiles(folderPath).catch((e) => Logger.error(e));
  }

  private async extractImageUrl(photos: Photos): Promise<string> {
    const extractedPhoto = photos[photos.length - 1];
    return (await this.bot.telegram.getFileLink(extractedPhoto.file_id)).href.toString();
  }
}
