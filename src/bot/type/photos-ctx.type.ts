export type CtxWithPhoto = {
  message: {
    photo: Photos;
  };
};

export type Photos = [{ file_id: string; file_unique_id: string }];
