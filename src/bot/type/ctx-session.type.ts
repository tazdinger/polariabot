export type CtxSession = {
  session: {
    isReplySent: boolean;
    lastMessage: number;
    page: number;
    mediaMessageId: number[];
  };
};
