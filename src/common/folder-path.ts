import * as path from 'path';

export const STATIC_FOLDER = path.join(__dirname, '..', '..', 'static');

export const folder = {
  schedule: path.join(STATIC_FOLDER, folderType.schedule),
  sketches: path.join(STATIC_FOLDER, folderType.sketches),
  portfolio: path.join(STATIC_FOLDER, folderType.portfolio),
};

const enum folderType {
  schedule = 'schedule',
  sketches = 'sketches',
  portfolio = 'portfolio',
}
