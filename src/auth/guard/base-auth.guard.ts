import {
  CanActivate,
  ExecutionContext,
  Injectable,
  Logger,
  UnauthorizedException,
} from '@nestjs/common';
import { Request } from 'express';

@Injectable()
export class BaseAuthGuard implements CanActivate {
  canActivate(context: ExecutionContext): boolean | Promise<boolean> {
    const request: Request = context.switchToHttp().getRequest();
    const authHeader = request.headers.authorization;
    const user = atob(authHeader.split(' ')[1]).split(':');
    if (!user) {
      Logger.error('Неудачная попытка входа');
      throw new UnauthorizedException();
    }
    if (user[0] === process.env.USER_NAME && user[1] === process.env.USER_PASSWORD) {
      return true;
    }
    Logger.error('Неудачная попытка входа');
    throw new UnauthorizedException();
  }
}
