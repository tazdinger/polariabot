import { Module } from '@nestjs/common';
import { WhitelistMiddleware } from './whitelist.middleware';

@Module({
  providers: [WhitelistMiddleware],
  exports: [WhitelistMiddleware],
})
export class MiddlewareModule {}
