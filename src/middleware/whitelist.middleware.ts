import { Injectable } from '@nestjs/common';
import { NextFunction } from 'express';
import { Context } from 'telegraf';

@Injectable()
export class WhitelistMiddleware {
  use(ctx: Context, next: NextFunction) {
    const whitelist = [464949683, 6056476856];

    if (!whitelist.includes(ctx.from.id)) {
      ctx.reply('Тебе сюда нельзя');
      return;
    }

    next();
  }
}
