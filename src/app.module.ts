import { Module } from '@nestjs/common';
import { BotModule } from './bot/bot.module';
import { FileModule } from './file/file.module';
import { ConfigModule } from '@nestjs/config';
import { MiddlewareModule } from './middleware/middleware.module';

@Module({
  imports: [
    BotModule,
    FileModule,
    ConfigModule.forRoot({
      isGlobal: true,
      load: [],
    }),
    MiddlewareModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
