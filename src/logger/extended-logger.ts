import { ConsoleLogger } from '@nestjs/common';
import logger from './winston-logger';

export class ExtendedLogger extends ConsoleLogger {
  error(message: any, ...optionalParams: any[]) {
    logger.error(`[${optionalParams['stack']}]: ${message}`);
    super.error(message, ...optionalParams);
  }

  log(message: any, ...optionalParams: any[]) {
    const stack = optionalParams[0];
    logger.info(`[${stack}]: ${message}`);
    super.log(message, ...optionalParams);
  }
}
