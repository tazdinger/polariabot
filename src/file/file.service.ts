import { Injectable, Logger } from '@nestjs/common';
import * as fsSync from 'fs';
import * as fs from 'fs/promises';
import * as path from 'path';
import * as https from 'https';

@Injectable()
export class FileService {
  public async saveFromUrl(url: string, dist: string): Promise<void> {
    await fs.stat(dist).catch(async () => await fs.mkdir(dist, { recursive: true }));
    const tmp = url.split('/');
    const fileName = tmp[tmp.length - 1];
    const file = fsSync.createWriteStream(path.join(dist, fileName));
    https
      .get(url, (response) => {
        response.pipe(file);
        file.on('finish', () => {
          file.close(() => {
            Logger.log('File downloaded successfully');
          });
        });
      })
      .on('error', (err) => {
        Logger.error(err);
        fs.unlink(dist);
      });
  }

  public async getFiles(folderPath: string): Promise<string[]> {
    return await fs.readdir(folderPath, { encoding: 'utf-8' });
  }

  public async deleteFiles(folderPath: string): Promise<void> {
    await fs.readdir(folderPath).then((file) => {
      file.forEach((f) => {
        fs.unlink(path.join(folderPath, f));
      });
    });
  }

  public async dateOfCreate(filePath: string): Promise<Date> {
    try {
      const stats = await fs.stat(filePath);
      return stats.mtime; // Время последнего изменения
    } catch (error) {
      Logger.error(`Ошибка при получении информации о файле: ${error.message}`);
      return null;
    }
  }
}
